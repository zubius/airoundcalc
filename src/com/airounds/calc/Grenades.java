package com.airounds.calc;

import java.util.ArrayList;

import android.util.SparseIntArray;

public class Grenades extends AStrikeStuff {

	SparseIntArray m26;
	SparseIntArray s26;
	SparseIntArray k26;
	ArrayList<SparseIntArray> gren;
	int gr, sb, kr;
	
	int price_gr_count;
	int price_sbor_count;
	int price_korp_count;
	int in_gr_count;
	int in_sb_count;
	int in_kr_count;
	int inprice_gr;
	int inprice_sbor;
	int inprice_korp;
	String call = "";
	int total, prib, my_prib;

	int g_counts[];
	int kom_counts[];
	
	public Grenades(ArrayList<SparseIntArray> gren,
			int[] g_counts, int[] kom_counts, int[] g_prices, int[] s_prices, int[] k_prices,
			int[] price_count, int in_count, int[] inprice) {
		
		m26 = new SparseIntArray(g_counts.length);
		s26 = new SparseIntArray(kom_counts.length);
		k26 = new SparseIntArray(kom_counts.length);
		
		this.price_gr_count = price_count[0];
		this.price_sbor_count = price_count[1];
		this.price_korp_count = price_count[2];
		this.in_gr_count = in_count;
		this.in_sb_count = in_count;
		this.in_kr_count = in_count;
		this.inprice_gr = inprice[0];
		this.inprice_sbor = inprice[1];
		this.inprice_korp = inprice[2];
		this.g_counts = g_counts;
		this.kom_counts = kom_counts;
		
		this.gren = initPrices(gren, g_counts, kom_counts, g_prices, s_prices, k_prices);
	}
	
	@Override
	public void calculate() {
		
		int price_gr, price_sbor, price_korp,
		gr_total, sb_total, kr_total, total_ms,
		gr_intotal, sb_intotal, kr_intotal, total_in_ms, total_in,
		prib_ms, prib_k, my_prib_ms, my_prib_k;
		String call = "";
		
		if (gr != 0) {
    		call += "�� " + gr + " ";
    	} if (sb != 0) {
    		call += "���� " + sb + " ";
    	} if (kr != 0) {
    		call += "���� " + kr + " ";
    	}
    	
    	this.call = call;
    	
    	price_gr_count = getPriceCount(gr, g_counts, price_gr_count, in_gr_count);
    	price_sbor_count = getPriceCount(sb, kom_counts, price_sbor_count, in_sb_count);
    	price_korp_count = getPriceCount(kr, kom_counts, price_korp_count, in_kr_count);
    	
    	price_gr = m26.get(price_gr_count);
    	price_sbor = s26.get(price_sbor_count);
    	price_korp = k26.get(price_korp_count);
    	
    	gr_total = price_gr * gr;
    	sb_total = price_sbor * sb;
    	kr_total = price_korp * kr;
    	total_ms = gr_total + sb_total;
    	total = gr_total + sb_total + kr_total;
    	
    	//Pribil
    	gr_intotal = inprice_gr * gr;
    	sb_intotal = inprice_sbor * sb;
    	kr_intotal = inprice_korp * kr;
    	total_in_ms = gr_intotal + sb_intotal;
    	total_in = gr_intotal + sb_intotal + kr_intotal;
    	
    	// gr & sb - 50% jf prib to AK; kr - all prib to us;
    	prib = total - total_in;
    	prib_ms = total_ms - total_in_ms;
    	prib_k = kr_total - kr_intotal;
    	my_prib_ms = prib_ms / 4;
    	my_prib_k = prib_k / 2;
    	my_prib = my_prib_ms + my_prib_k;
	}

	@Override
	public String getCall() {
		return call;
	}

	@Override
	public int getTotal() {
		return total;
	}

	@Override
	public int getPrib() {
		return prib;
	}

	@Override
	public int getMyPrib() {
		return my_prib;
	}

	public ArrayList<SparseIntArray> initPrices(ArrayList<SparseIntArray> gren, 
			int[] g_counts, int[] kom_counts, int[] g_prices, int[] s_prices, int[] k_prices) {
		
		
		for (short i = 0; i < g_counts.length; i++) {
			m26.put(g_counts[i], g_prices[i]);
		}
		gren.set(0, m26);
		
		for (short i = 0; i < g_counts.length; i++) {
			s26.put(kom_counts[i], s_prices[i]);
		}
		gren.set(1, s26);
		
		for (short i = 0; i < g_counts.length; i++) {
			k26.put(kom_counts[i], k_prices[i]);
		}
		gren.set(2, k26);
		
		return gren;
	}
	
	public int getPriceCount(int count, int[] counts,
    		int price_c, int in_count) {
		return super.getPriceCount(count, counts, price_c, in_count);
	}

	@Override
	public void setCounts(int[] counts) {

		gr = counts[0];
		sb = counts[1];
		kr = counts[2];
	}
}
