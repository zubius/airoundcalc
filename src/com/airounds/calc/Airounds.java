package com.airounds.calc;

import java.util.ArrayList;
import android.util.SparseIntArray;

public class Airounds extends AStrikeStuff {

	SparseIntArray s25;
	SparseIntArray s28;
	SparseIntArray trass;
	ArrayList<SparseIntArray> round;
	
	int i25p, i28p, i20tp, i25tp, i25k, i28k, i20tk, i25tk;
	
	int kor_count;
	int price_count;
	int in_count;
	int a_counts[];
	int a25_prices[];
	int a28_prices[];
	int trs_prices[];
	String call = "";
	int total, ipribil, imy_prib;
	
	public Airounds(ArrayList<SparseIntArray> round, int[] a_counts, int[] a25_prices,
			int[] a28_prices, int[] trs_prices, int kor_count, int price_count, int in_count) {
		
		s25 = new SparseIntArray(a_counts.length);
		s28 = new SparseIntArray(a_counts.length);
		trass = new SparseIntArray(a_counts.length);
		
		this.kor_count = kor_count;
		this.price_count = price_count;
		this.in_count = in_count;
		this.a_counts = a_counts;
		this.round = initPrices(round, a_counts, a25_prices, a28_prices, trs_prices);
	}

	@Override
	public void calculate() {
		
		int i25, i28, i20t, i25t, iall,
		i25_price, i28_price, i20t_price, i25t_price,
		i25_total, i28_total, i20t_total, i25t_total,
		i25_inprice, i28_inprice, i20t_inprice, i25t_inprice,
		i25_intotal, i28_intotal, i20t_intotal, i25t_intotal, total_in;
		String call = "";
		
		i25 = i25p + i25k * kor_count;
    	i28 = i28p + i28k * kor_count;
    	i20t = i20tp + i20tk * kor_count;
    	i25t = i25tp + i25tk * kor_count;
    	iall = i25 + i28 + i20t + i25t;
    	
    	if (i25 != 0) {
    		call += "0.25 - " + i25 + " ";
    	} if (i28 != 0) {
    		call += "0.28 - " + i28 + " ";
    	} if (i20t != 0) {
    		call += "0.20t - " + i20t + " ";
    	} if (i25t != 0) {
    		call += "0.25t - " + i25t + " ";
    	}
    	
    	this.call = call;
    	
    	price_count = getPriceCount(iall, a_counts, price_count, in_count);
    	
    	i25_price = s25.get(price_count);
    	i28_price = s28.get(price_count);
    	i20t_price = trass.get(price_count);
    	i25t_price = trass.get(price_count);
    	
    	i25_total = i25_price * i25;
    	i28_total = i28_price * i28;
    	i20t_total = i20t_price * i20t;
    	i25t_total = i25t_price * i25t;
    	total = i25_total + i28_total + i20t_total + i25t_total;
    	
    	//Pribil
    	i25_inprice = s25.get(in_count);
    	i28_inprice = s28.get(in_count);
    	i20t_inprice = trass.get(in_count);
    	i25t_inprice = trass.get(in_count);
    	
    	i25_intotal = i25_inprice * i25;
    	i28_intotal = i28_inprice * i28;
    	i20t_intotal = i20t_inprice * i20t;
    	i25t_intotal = i25t_inprice * i25t;
    	total_in = i25_intotal + i28_intotal + i20t_intotal + i25t_intotal;
    	
    	ipribil = total - total_in;
    	imy_prib = (int) ((ipribil * 0.75) / 2);
	}

	@Override
	public String getCall() {
		return call;
	}
	
	@Override
	public int getTotal() {
		return total;
	}

	@Override
	public int getPrib() {
		return ipribil;
	}

	@Override
	public int getMyPrib() {
		return imy_prib;
	}

	private ArrayList<SparseIntArray> initPrices(ArrayList<SparseIntArray> round, 
			int[] a_counts, int[] a25_prices, int[] a28_prices, int[] trs_prices) {
		
		for (short i = 0; i < a_counts.length; i++) {
			s25.put(a_counts[i], a25_prices[i]);
		}
		round.set(0, s25);
		
		for (short i = 0; i < a_counts.length; i++) {
			s28.put(a_counts[i], a28_prices[i]);
		}
		round.set(1, s28);
		
		for (short i = 0; i < a_counts.length; i++) {
			trass.put(a_counts[i], trs_prices[i]);
		}
		round.set(2, trass);
		
		return round;
	}

	public int getPriceCount(int count, int[] counts,
    		int price_c, int in_count) {
		return super.getPriceCount(count, counts, price_c, in_count);
	}

	@Override
	public void setCounts(int[] counts) {

		i25p = counts[0];
		i28p = counts[1];
		i20tp = counts[2];
		i25tp = counts[3];
		i25k = counts[4];
		i28k = counts[5];
		i20tk = counts[6];
		i25tk = counts[7];
	}
}
