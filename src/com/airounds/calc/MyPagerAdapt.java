package com.airounds.calc;

import java.util.List;

import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

public class MyPagerAdapt extends PagerAdapter {

	List<View> pages = null;
	
	public MyPagerAdapt(List<View> pages) {
		this.pages = pages;
	}
	
	@Override
	public Object instantiateItem(View col, int pos) {
		View v = pages.get(pos);
		((ViewPager) col).addView(v, 0);
		return v;
	}
	
	@Override
	public void destroyItem(View col, int pos, Object view) {
		((ViewPager)col).removeView((View)view);
	}
	
	@Override
	public int getCount() {
		return pages.size();
	}

	@Override
	public boolean isViewFromObject(View view, Object obj) {
		return view.equals(obj);
	}
	
	@Override
	public void finishUpdate(View v) {
	}
	
	@Override
	public void restoreState(Parcelable parc, ClassLoader cl) {
	}
	
	@Override
	public Parcelable saveState() {
		return null;
	}
	
	@Override
	public void startUpdate(View v) {
	}

}
