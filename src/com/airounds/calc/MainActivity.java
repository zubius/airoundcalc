package com.airounds.calc;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.SparseIntArray;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.app.Activity;

public class MainActivity extends Activity {

	SparseIntArray s25 = new SparseIntArray(7);
	SparseIntArray s28 = new SparseIntArray(7);
	SparseIntArray trass = new SparseIntArray(7);
	ArrayList<SparseIntArray> round = new ArrayList<SparseIntArray>();
	
	final int a_counts[] = {12, 24, 48, 96, 240, 480, 960};
	final int a25_prices[] = {400, 360, 350, 320, 300, 290, 280};
	final int a28_prices[] = {400, 370, 360, 340, 320, 310, 300};
	final int trs_prices[] = {1000, 950, 900, 850, 800, 750, 700};
	final int kor_count = 24;
	final int a_price_count = 12;
	final int in_count = 960;
	Airounds airounds;
	
	SparseIntArray m26 = new SparseIntArray(3);
	SparseIntArray s26 = new SparseIntArray(5);
	SparseIntArray k26 = new SparseIntArray(5);
	ArrayList<SparseIntArray> gren = new ArrayList<SparseIntArray>();
	//9000 is my incoming price
	final int g_counts[] = {30, 50, 100, 9000};
	final int kom_counts[] = {50, 100, 500, 1000, 3000, 9000};
	final int gr_prices[] = {100, 95, 85, 65};
	final int sb_prices[] = {65, 60, 55, 50, 45, 40};
	final int kr_prices[] = {30, 25, 20, 18, 15, 12};
	final int inprice[] = {65, 40, 12};
	final int g_price_count[] = {30, 50, 50};
	Grenades grenades;
	
	Button chk, rst, gr_chk, gr_rst;
	EditText s25p_et, s28p_et, t20p_et, t25p_et, s25k_et, s28k_et, t20k_et, t25k_et,
				gr_count, sbor_count, korp_count;
	TextView zakaz, cena, pribil, my_prib,
				gr_zakaz, gr_cena, gr_prib, gr_my_prib;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
        mapCol();
        airounds = new Airounds(round, a_counts, a25_prices, a28_prices, trs_prices, kor_count,
        		a_price_count, in_count);
        grenades = new Grenades(gren, g_counts, kom_counts, gr_prices, sb_prices, kr_prices,
        		g_price_count, in_count, inprice);
        
        LayoutInflater inflate = LayoutInflater.from(this);
        List<View> pages = new ArrayList<View>();
        
        View aironds = inflate.inflate(R.layout.activity_main, null);
        chk = (Button)aironds.findViewById(R.id.chk_bt);
        rst = (Button)aironds.findViewById(R.id.rst_bt);
        s25p_et = (EditText)aironds.findViewById(R.id.s25_p_et);
        s28p_et = (EditText)aironds.findViewById(R.id.s28_p_ed);
        t20p_et = (EditText)aironds.findViewById(R.id.t20_p_et);
        t25p_et = (EditText)aironds.findViewById(R.id.t25_p_ed);
        s25k_et = (EditText)aironds.findViewById(R.id.s25_k_et);
        s28k_et = (EditText)aironds.findViewById(R.id.s28_k_ed);
        t20k_et = (EditText)aironds.findViewById(R.id.t20_k_ed);
        t25k_et = (EditText)aironds.findViewById(R.id.t25_k_ed);
        zakaz = (TextView)aironds.findViewById(R.id.zakaz_tv);
        cena = (TextView)aironds.findViewById(R.id.cena_tv);
        pribil = (TextView)aironds.findViewById(R.id.pribil_tv);
        my_prib = (TextView)aironds.findViewById(R.id.my_prib_tv);
        chk.setOnClickListener(air_bt_listener);
        rst.setOnClickListener(air_bt_listener);
        s25p_et.setOnKeyListener(onEnterCheck);
        s28p_et.setOnKeyListener(onEnterCheck);
        t20p_et.setOnKeyListener(onEnterCheck);
        t25p_et.setOnKeyListener(onEnterCheck);
        s25k_et.setOnKeyListener(onEnterCheck);
        s28k_et.setOnKeyListener(onEnterCheck);
        t20k_et.setOnKeyListener(onEnterCheck);
        t25k_et.setOnKeyListener(onEnterCheck);
        pages.add(aironds);
        
        View gren = inflate.inflate(R.layout.grenades, null);
        gr_chk = (Button)gren.findViewById(R.id.gr_chk_bt);
        gr_rst = (Button)gren.findViewById(R.id.gr_rst_bt);
        gr_count = (EditText)gren.findViewById(R.id.gr_count);
        sbor_count = (EditText)gren.findViewById(R.id.sbor_count);
        korp_count = (EditText)gren.findViewById(R.id.korp_count);
        gr_zakaz = (TextView)gren.findViewById(R.id.gr_zakaz_tv);
        gr_cena = (TextView)gren.findViewById(R.id.gr_cena_tv);
        gr_prib = (TextView)gren.findViewById(R.id.gr_prib_tv);
        gr_my_prib = (TextView)gren.findViewById(R.id.gr_my_prib_tv);
        gr_count.requestFocus();
        gr_chk.setOnClickListener(gr_bt_listener);
        gr_rst.setOnClickListener(gr_bt_listener);
        gr_count.setOnKeyListener(onGrEnterCheck);
        sbor_count.setOnKeyListener(onGrEnterCheck);
        korp_count.setOnKeyListener(onGrEnterCheck);
        pages.add(gren);
        
        MyPagerAdapt pageAd = new MyPagerAdapt(pages);
        ViewPager vp = new ViewPager(this);
        vp.setAdapter(pageAd);
        
        setContentView(vp);
    }
    
    OnClickListener air_bt_listener = new OnClickListener() {
    	
    	public void onClick(View v) {
    		
    		if (v.getId() == R.id.chk_bt) {
    			airCalculate();
    		} else if (v.getId() == R.id.rst_bt) {
    			airResetButton();
    		}
    	}
    };
    
    OnClickListener gr_bt_listener = new OnClickListener() {
    	
    	public void onClick(View v) {
    		
    		if (v.getId() == R.id.gr_chk_bt) {
    			grCalculate();
    		} else if (v.getId() == R.id.gr_rst_bt) {
    			grResetButton();
    		}
    	}
    };
    
    OnKeyListener onEnterCheck = new OnKeyListener() {
    	
    	public boolean onKey (View v, int keyCode, KeyEvent event) {
    		
    		if (keyCode == KeyEvent.KEYCODE_ENTER) {
    			//airCalculate();
    			return true;
    		}
    		return false;
    	}
    };
    
    OnKeyListener onGrEnterCheck = new OnKeyListener() {
    	
    	public boolean onKey (View v, int keyCode, KeyEvent event) {
    		
    		if (keyCode == KeyEvent.KEYCODE_ENTER) {
    			//grCalculate();
    			return true;
    		}
    		return false;
    	}
    };
    
/*    public boolean onKeyDown (View v, int keyCode, KeyEvent event) {
    	
    	if (keyCode == KeyEvent.KEYCODE_ENTER) {
    		airCalculate();
    		return true;
    	}
    	return super.onKeyDown(keyCode, event);
    }*/
    
    private void mapCol() {
    	
    	round.add(s25);
        round.add(s28);
        round.add(trass);
        
        gren.add(m26);
        gren.add(s26);
        gren.add(k26);
    }

    private void airCalculate() {
    	
    	int i25p, i28p, i20tp, i25tp, i25k, i28k, i20tk, i25tk;
    	
    	// count of rounds
    	i25p = Integer.parseInt(s25p_et.getText().toString());
    	i28p = Integer.parseInt(s28p_et.getText().toString());
    	i20tp = Integer.parseInt(t20p_et.getText().toString());
    	i25tp = Integer.parseInt(t25p_et.getText().toString());
    	i25k = Integer.parseInt(s25k_et.getText().toString());
    	i28k = Integer.parseInt(s28k_et.getText().toString());
    	i20tk = Integer.parseInt(t20k_et.getText().toString());
    	i25tk = Integer.parseInt(t25k_et.getText().toString());
    	
    	int counts[] = {i25p, i28p, i20tp, i25tp, i25k, i28k, i20tk, i25tk};
    	airounds.setCounts(counts);
    	airounds.calculate();
    	
    	zakaz.setText(airounds.getCall());
    	cena.setText(airounds.getTotal() + R.string.rub);
    	pribil.setText(airounds.getPrib() + R.string.rub);
    	my_prib.setText(airounds.getMyPrib() + R.string.rub);
    }

    private void grCalculate() {
    	int gr, sb, kr;
    	
    	//count of pyross
    	gr = Integer.parseInt(gr_count.getText().toString());
    	sb = Integer.parseInt(sbor_count.getText().toString());
    	kr = Integer.parseInt(korp_count.getText().toString());
    	
    	int counts[] = {gr, sb, kr};
    	grenades.setCounts(counts);
    	grenades.calculate();
    	
    	gr_zakaz.setText(grenades.getCall());
    	gr_cena.setText(grenades.getTotal() + R.string.rub);
    	gr_prib.setText(grenades.getPrib() + R.string.rub);
    	gr_my_prib.setText(grenades.getMyPrib() + R.string.rub);
    }
    
    private void airResetButton() {
    	
    	s25p_et.setText(R.string.zero);
		s28p_et.setText(R.string.zero);
		t20p_et.setText(R.string.zero);
		t25p_et.setText(R.string.zero);
		s25k_et.setText(R.string.zero);
		s28k_et.setText(R.string.zero);
		t20k_et.setText(R.string.zero);
		t25k_et.setText(R.string.zero);
		
		zakaz.setText("");
		cena.setText("");
		pribil.setText("");
		my_prib.setText("");
		
		s25p_et.requestFocus();
    }
    
    private void grResetButton() {
    	
    	gr_count.setText(R.string.zero);
		sbor_count.setText(R.string.zero);
		korp_count.setText(R.string.zero);
		
		gr_zakaz.setText("");
		gr_cena.setText("");
		gr_prib.setText("");
		gr_my_prib.setText("");
		
		gr_count.requestFocus();
    }
}
