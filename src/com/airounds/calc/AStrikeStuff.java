package com.airounds.calc;

public abstract class AStrikeStuff {
	
	public abstract void calculate();
	
	public abstract String getCall();
	
	public abstract int getTotal();
	
	public abstract int getPrib();
	
	public abstract int getMyPrib();
	
	public abstract void setCounts(int[] arg);
	
	protected int getPriceCount(int count, int[] counts,
    		int price_c, int in_count) {
    	
    	for (short i = 0; i < counts.length; i++) {
    		if (count == 0) {	
    			break;
    		} else if (count <= counts[0] && count != 0) {
    			price_c = counts[0];
    			break;
    		} else if (count < counts[i] && count != 0) {	
    			price_c = counts[i - 1];
    			break;
    		} else if (count == counts[i]) {
    			price_c = counts[i];
    			break;
    		} else if (count >= in_count) {
    			price_c = in_count;
    			break;
    		}
    	}
    	
    	return price_c;
    }
}
