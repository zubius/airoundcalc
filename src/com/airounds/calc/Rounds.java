package com.airounds.calc;

import android.util.SparseIntArray;

public class Rounds extends AStrikeStuff {
	
	int in_kor_count;
	int price_count;
	int incom_count;
	int pack_count, korob_count;
	int a_counts[];
	int prices[];
	int total;
	SparseIntArray rounds;
	
	public Rounds(int pack_count, int korob_count, int[] a_counts, int[] prices, int kor_count, int price_count, int in_count) {
		
		rounds = new SparseIntArray(a_counts.length);
		
		this.pack_count = pack_count;
		this.korob_count= korob_count;
		this.in_kor_count = kor_count;
		this.price_count = price_count;
		this.incom_count = in_count;
		this.a_counts = a_counts;
		rounds = initPrices(rounds, this.a_counts, this.prices);
	}
	
	private SparseIntArray initPrices(SparseIntArray rounds, int[] a_counts, int[] prices) {
		
		for (short i = 0; i < a_counts.length; i++) {
			rounds.put(a_counts[i], prices[i]);
		}
		
		return rounds;
	}
	
	public int getPriceCount(int count, int[] counts,
    		int price_c, int in_count) {
		return super.getPriceCount(count, counts, price_c, in_count);
	}

	@Override
	public void calculate() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getCall() {
		return "";
	}
	
	@Override
	public int getTotal() {
		return total;
	}

	@Override
	public int getPrib() {
		return 0;
	}

	@Override
	public int getMyPrib() {
		return 0;
	}

	@Override
	public void setCounts(int[] arg) {
		// TODO Auto-generated method stub
		
	}

	
}
